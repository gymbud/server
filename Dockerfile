FROM openjdk:11-slim as BUILD

COPY . /home/gradle/project

WORKDIR /home/gradle/project

RUN ./gradlew -q --no-daemon shadowJar

FROM openjdk:11-jre-slim

ENV DB_HOST db

RUN mkdir /project

WORKDIR /project

COPY --from=BUILD /home/gradle/project/build/libs/server-0.0.1.jar ./app.jar

EXPOSE 8000

CMD ["java", "-server", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "app.jar"]
