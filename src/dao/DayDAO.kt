package xyz.gofdev.dao

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.gofdev.model.*

object DayTable : Table() {
    val id = integer("id").autoIncrement()
    val name = text("name").uniqueIndex()
    val description = text("description").nullable()
    val idPlan = reference("id_plan", PlanTable.id)

    override val primaryKey = PrimaryKey(id)
}

interface DayDAO {
    suspend fun getAll(): Collection<Day>
    suspend fun getAllByPlanId(idPlan: Int): Collection<Day>
    suspend fun getById(id: Int): Day?
    suspend fun getOwnerById(id: Int): User?
    suspend fun getOwnerByPlanId(id: Int): User?
    suspend fun create(day: Day): Day
    suspend fun delete(id: Int): Boolean
}

class DayDAOImpl(val db: Database) : DayDAO {
    override suspend fun getAll(): Collection<Day> = transaction(db) {
        (DayTable innerJoin PlanTable innerJoin UserTable)
            .selectAll()
            .map {
                val owner = User(
                    id = it[UserTable.id],
                    firstName = it[UserTable.firstName],
                    lastName = it[UserTable.lastName],
                    email = it[UserTable.mail]
                )

                val plan = Plan(
                    id = it[PlanTable.id],
                    name = it[PlanTable.name],
                    description = it[PlanTable.description],
                    duration = it[PlanTable.duration],
                    owner = owner
                )

                Day(
                    id = it[DayTable.id],
                    name = it[DayTable.name],
                    description = it[DayTable.description],
                    plan = plan
                )
            }
    }

    override suspend fun getAllByPlanId(idPlan: Int): Collection<Day> = transaction(db) {
        (DayTable innerJoin PlanTable)
            .select { DayTable.idPlan eq idPlan }
            .map {
                val plan = Plan(
                    id = it[PlanTable.id],
                    name = it[PlanTable.name],
                    description = it[PlanTable.description],
                    duration = it[PlanTable.duration]
                )

                Day(
                    id = it[DayTable.id],
                    name = it[DayTable.name],
                    description = it[DayTable.description],
                    plan = plan
                )
            }
    }

    override suspend fun getById(id: Int): Day? = transaction(db) {
        (DayTable innerJoin PlanTable innerJoin UserTable)
            .select { DayTable.id eq id }
            .map {
                val owner = User(
                    id = it[UserTable.id],
                    firstName = it[UserTable.firstName],
                    lastName = it[UserTable.lastName],
                    email = it[UserTable.mail]
                )

                val plan = Plan(
                    id = it[PlanTable.id],
                    name = it[PlanTable.name],
                    description = it[PlanTable.description],
                    duration = it[PlanTable.duration],
                    owner = owner
                )

                Day(
                    id = it[DayTable.id],
                    name = it[DayTable.name],
                    description = it[DayTable.description],
                    plan = plan
                )
            }.firstOrNull()
    }

    override suspend fun getOwnerById(id: Int): User? = transaction(db) {
        (DayTable innerJoin PlanTable innerJoin UserTable)
            .select { DayTable.id eq id }
            .map { it.toUser() }
            .firstOrNull()
    }

    override suspend fun getOwnerByPlanId(id: Int): User? = transaction(db) {
        (PlanTable innerJoin UserTable)
            .select { PlanTable.id eq id }
            .map { it.toUser() }
            .firstOrNull()
    }

    override suspend fun create(day: Day): Day = transaction(db) {
        val id = DayTable
            .insert {
                it[idPlan] = day.planId!!
                it[name] = day.name
                it[description] = day.description
            }[DayTable.id]

        Day(
            id = id,
            name = day.name,
            description = day.description
        )
    }

    override suspend fun delete(id: Int): Boolean = transaction(db) {
        DayTable.deleteWhere { DayTable.id eq id } > 0
    }

}

fun ResultRow.toDay(specifics: Collection<Specific>? = null): Day {
    return Day(
        id = this[DayTable.id],
        name = this[DayTable.name],
        description = this[DayTable.description],
        exercices = specifics
    )

}