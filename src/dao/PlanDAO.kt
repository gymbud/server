package xyz.gofdev.dao

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.gofdev.model.Day
import xyz.gofdev.model.Exercice
import xyz.gofdev.model.Plan
import xyz.gofdev.model.User

object PlanTable : Table() {
    val id = integer("id").autoIncrement()
    val name = text("name")
    val duration = integer("duration")
    val description = text("description").nullable()
    val userId = reference("id_user", UserTable.id)

    override val primaryKey = PrimaryKey(id)
}

interface PlanDAO {
    suspend fun getAll(): Collection<Plan>
    suspend fun getById(id: Int): Plan?
    suspend fun getByUserId(id: Int): Collection<Plan>
    suspend fun getOwnerById(id: Int): User?
    suspend fun create(plan: Plan, owner: User): Plan
    suspend fun delete(id: Int): Boolean
}

class PlanDAOImpl(val db: Database) : PlanDAO {
    override suspend fun getAll(): Collection<Plan> = transaction(db) {
        PlanTable
            .selectAll()
            .map { it.toPlan() }
    }

    override suspend fun getById(id: Int): Plan? = transaction(db) {
        val days = DayTable.select { DayTable.idPlan eq id }.map { it.toDay() }
        PlanTable
            .select { PlanTable.id eq id }
            .map {
                it.toPlan(days)
            }
            .firstOrNull()
    }

    override suspend fun getByUserId(id: Int): Collection<Plan> = transaction(db) {
        PlanTable.select { PlanTable.userId eq id }.map {
            val days = DayTable
                .select { DayTable.idPlan eq it[PlanTable.id] }
                .map { dayRow -> dayRow.toDay() }
            it.toPlan(days)
        }
    }

    override suspend fun getOwnerById(id: Int): User? = transaction(db) {
        (PlanTable innerJoin UserTable)
            .select { PlanTable.id eq id }
            .map { it.toUser() }
            .firstOrNull()
    }

    override suspend fun create(plan: Plan, owner: User): Plan = transaction(db) {
        val id = PlanTable.insert {
            it[name] = plan.name
            it[description] = plan.description
            it[duration] = plan.duration
            it[userId] = owner.id
        }[PlanTable.id]

        Plan(
            id = id,
            name = plan.name,
            description = plan.description,
            duration = plan.duration,
            owner = owner
        )
    }

    override suspend fun delete(id: Int): Boolean = transaction(db) {
        PlanTable
            .deleteWhere { PlanTable.id eq id } > 0
    }

}

fun ResultRow.toPlan(planDays: Collection<Day>? = null): Plan {
    return Plan(
        id = this[PlanTable.id],
        name = this[PlanTable.name],
        duration = this[PlanTable.duration],
        description = this[PlanTable.description],
        days = planDays
    )
}
