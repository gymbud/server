package xyz.gofdev.dao

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.*
import xyz.gofdev.model.*

object LoginTable : Table() {
    val id = integer("id").autoIncrement()
    val userId = reference("id_user", UserTable.id).uniqueIndex()
    val token = text("token")

    override val primaryKey = PrimaryKey(id)
}

interface LoginDAO {

    suspend fun getToken(user: User): String?
    suspend fun login(user: User, token: String)
    suspend fun logout(user: User): Boolean
}

class LoginDAOImpl(private val db: Database) : LoginDAO {

    override suspend fun getToken(user: User): String? = transaction(db) {
        LoginTable.select { LoginTable.userId eq user.id }.firstOrNull()?.get(LoginTable.token)
    }

    override suspend fun login(user: User, token: String) = transaction(db) {
        LoginTable
            .insert {
                it[this.userId] = user.id
                it[this.token] = token
            }
        Unit
    }

    override suspend fun logout(user: User): Boolean = transaction(db) {
        LoginTable.deleteWhere { LoginTable.userId eq user.id }
    } > 0
}
