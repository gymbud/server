package xyz.gofdev.dao

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.gofdev.model.Day
import xyz.gofdev.model.Exercice
import xyz.gofdev.model.Specific
import xyz.gofdev.model.User

object SpecificTable : Table() {
    val id = integer("id").autoIncrement()
    val dayId = reference("id_day", DayTable.id)
    val exerciceId = reference("id_exercice", ExerciceTable.id)
    val sets = integer("sets").nullable()
    val reps = integer("reps").nullable()
    val rest = integer("rest").nullable()
    val tempo = text("tempo").nullable()
    val duration = integer("duration").nullable()

    override val primaryKey = PrimaryKey(id)
}

interface SpecificDAO {
    suspend fun getAll(): Collection<Specific>
    suspend fun getAllByPlanId(id: Int): Collection<Specific>
    suspend fun getById(id: Int): Specific?
    suspend fun getOwnerById(id: Int): User?
    suspend fun getOwnerByDayId(id: Int): User?
    suspend fun create(specific: Specific): Specific
    suspend fun delete(id: Int): Boolean
}

class SpecificDAOImpl(val db: Database) : SpecificDAO {
    override suspend fun getAll(): Collection<Specific> = transaction(db) {
        (SpecificTable innerJoin DayTable innerJoin ExerciceTable)
            .selectAll()
            .map {
                val exercice = Exercice(
                    id = it[ExerciceTable.id],
                    name = it[ExerciceTable.name],
                    description = it[ExerciceTable.description]
                )

                val day = Day(
                    id = it[DayTable.id],
                    name = it[DayTable.name],
                    description = it[DayTable.description]
                )

                Specific(
                    id = it[SpecificTable.id],
                    duration = it[SpecificTable.duration],
                    sets = it[SpecificTable.sets],
                    reps = it[SpecificTable.reps],
                    tempo = it[SpecificTable.tempo],
                    rest = it[SpecificTable.rest],
                    exercice = exercice,
                    day = day
                )
            }
    }

    override suspend fun getAllByPlanId(id: Int): Collection<Specific> = transaction(db) {
        (SpecificTable innerJoin ExerciceTable innerJoin DayTable innerJoin PlanTable)
            .select { PlanTable.id eq id }
            .map {
                val exercice = Exercice(
                    id = it[ExerciceTable.id],
                    name = it[ExerciceTable.name]
                )

                val day = Day(
                    id = it[DayTable.id],
                    name = it[DayTable.name],
                    description = it[DayTable.description]
                )

                Specific(
                    id = it[SpecificTable.id],
                    duration = it[SpecificTable.duration],
                    sets = it[SpecificTable.sets],
                    reps = it[SpecificTable.reps],
                    tempo = it[SpecificTable.tempo],
                    rest = it[SpecificTable.rest],
                    exercice = exercice,
                    day = day
                )
            }
    }

    override suspend fun getById(id: Int): Specific? = transaction(db) {
        (SpecificTable innerJoin ExerciceTable innerJoin DayTable)
            .select { SpecificTable.id eq id }
            .map {
                val exercice = Exercice(
                    id = it[ExerciceTable.id],
                    name = it[ExerciceTable.name]
                )

                val day = Day(
                    id = it[DayTable.id],
                    name = it[DayTable.name],
                    description = it[DayTable.description]
                )

                Specific(
                    id = it[SpecificTable.id],
                    duration = it[SpecificTable.duration],
                    sets = it[SpecificTable.sets],
                    reps = it[SpecificTable.reps],
                    tempo = it[SpecificTable.tempo],
                    rest = it[SpecificTable.rest],
                    exercice = exercice,
                    day = day
                )
            }.firstOrNull()

    }

    override suspend fun getOwnerById(id: Int): User? = transaction(db) {
        (SpecificTable innerJoin DayTable innerJoin PlanTable innerJoin UserTable)
            .select { SpecificTable.id eq id }
            .map { it.toUser() }
            .firstOrNull()
    }

    override suspend fun getOwnerByDayId(id: Int): User? = transaction(db) {
        (DayTable innerJoin PlanTable innerJoin UserTable)
            .select { DayTable.id eq id }
            .map { it.toUser() }
            .firstOrNull()
    }

    override suspend fun create(specific: Specific): Specific = transaction(db) {
        val id = SpecificTable.insert {
            it[dayId] = specific.dayId!!
            it[exerciceId] = specific.exerciceId!!
            it[sets] = specific.sets
            it[reps] = specific.reps
            it[rest] = specific.rest
            it[tempo] = specific.tempo
            it[duration] = specific.duration
        }[SpecificTable.id]

        (SpecificTable innerJoin ExerciceTable innerJoin DayTable)
            .select { SpecificTable.id eq id }
            .map {
                val exercice = Exercice(
                    id = it[ExerciceTable.id],
                    name = it[ExerciceTable.name]
                )

                val day = Day(
                    id = it[DayTable.id],
                    name = it[DayTable.name],
                    description = it[DayTable.description]
                )

                Specific(
                    id = it[SpecificTable.id],
                    duration = it[SpecificTable.duration],
                    sets = it[SpecificTable.sets],
                    reps = it[SpecificTable.reps],
                    tempo = it[SpecificTable.tempo],
                    rest = it[SpecificTable.rest],
                    exercice = exercice,
                    day = day
                )
            }.first()
    }

    override suspend fun delete(id: Int): Boolean = transaction(db) {
        SpecificTable
            .deleteWhere {
                SpecificTable.id eq id
            } > 0
    }
}

