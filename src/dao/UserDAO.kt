package xyz.gofdev.dao

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.*
import xyz.gofdev.model.*
import at.favre.lib.crypto.bcrypt.*

object UserTable : Table() {
    val id = integer("id").autoIncrement()
    val firstName = text("first_name")
    val lastName = text("last_name")
    val mail = text("mail").uniqueIndex(customIndexName = "user_mail_uk")
    val pass = text("pass")

    override val primaryKey = PrimaryKey(id)
}

interface UserDAO {

    fun getAll(): List<User>

    fun getById(id: Int): User?

    fun getByMail(mail: String): User?

    fun create(user: User): User

    fun getByIdWithPassword(id: Int): User?

    fun getByEmailWithPassword(mail: String): User?
}

class UserDAOImpl(val db: Database) : UserDAO {

    override fun getAll(): List<User> = transaction(db) {
        UserTable
            .selectAll()
            .map {
                it.toUser()
            }
    }

    override fun getById(id: Int): User? = transaction(db) {
        UserTable
            .select { UserTable.id eq id }
            .map { it.toUser() }
            .firstOrNull()
    }


    override fun getByMail(mail: String): User? = transaction(db) {
        UserTable
            .select { UserTable.mail eq mail }
            .map { it.toUser() }
            .firstOrNull()
    }

    override fun getByIdWithPassword(id: Int): User? = transaction(db) {
        UserTable
            .select { UserTable.id eq id }
            .map { it.toUserWithPass() }
            .firstOrNull()
    }

    override fun getByEmailWithPassword(mail: String): User? = transaction(db) {
        UserTable
            .select { UserTable.mail eq mail }
            .map { it.toUserWithPass() }
            .firstOrNull()
    }

    override fun create(user: User): User = transaction(db) {
        val hash = BCrypt.withDefaults().hashToString(BCrypt.MIN_COST, user.password!!.toCharArray())

        val id = UserTable.insert {
            it[firstName] = user.firstName
            it[lastName] = user.lastName
            it[mail] = user.email
            it[pass] = hash
        }[UserTable.id]
        User(
            id = id,
            firstName = user.firstName,
            lastName = user.lastName,
            email = user.email
        )
    }
}

fun ResultRow.toUser(): User {
    return User(
        id = this[UserTable.id],
        firstName = this[UserTable.firstName],
        lastName = this[UserTable.lastName],
        email = this[UserTable.mail]
    )
}

fun ResultRow.toUserWithPass(): User {
    return User(
        id = this[UserTable.id],
        firstName = this[UserTable.firstName],
        lastName = this[UserTable.lastName],
        email = this[UserTable.mail],
        password = this[UserTable.pass]
    )
}

