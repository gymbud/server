package xyz.gofdev.dao

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.gofdev.model.Exercice
import xyz.gofdev.model.User

object ExerciceTable : Table() {
    val id = integer("id").autoIncrement()
    val userId = reference("id_user", UserTable.id)
    val name = text("name").uniqueIndex()
    val description = text("description").nullable()
}

interface ExerciceDAO {
    suspend fun getAll(): List<Exercice>
    suspend fun getById(id: Int): Exercice?
    suspend fun getOwnerById(id: Int): User?
    suspend fun create(exercice: Exercice, owner: User): Exercice
    suspend fun delete(id: Int, owner: User): Boolean
}

class ExerciceDAOImpl(private val db: Database) : ExerciceDAO {

    override suspend fun getAll(): List<Exercice> = transaction(db) {
        ExerciceTable
            .selectAll()
            .map { it.toExercice() }
    }

    override suspend fun getById(id: Int): Exercice? = transaction(db) {
        ExerciceTable
            .select { ExerciceTable.id eq id }
            .map { it.toExercice() }
            .firstOrNull()
    }

    override suspend fun getOwnerById(id: Int): User? = transaction(db) {
        (ExerciceTable innerJoin UserTable)
            .select { ExerciceTable.id eq id}
            .map { it.toUser() }
            .firstOrNull()
    }

    override suspend fun create(exercice: Exercice, owner: User) = transaction(db) {
        val id = ExerciceTable.insert {
            it[name] = exercice.name
            it[description] = exercice.description
            it[userId] = owner.id
        }[ExerciceTable.id]

        Exercice(
            id = id,
            name = exercice.name,
            description = exercice.description,
            owner = owner
        )
    }

    override suspend fun delete(id: Int, owner: User): Boolean = transaction(db) {
        ExerciceTable.deleteWhere {
            (ExerciceTable.id eq id) and (ExerciceTable.userId eq owner.id)
        } > 0
    }
}

fun ResultRow.toExercice(): Exercice {
    return Exercice(
        id = this[ExerciceTable.id],
        name = this[ExerciceTable.name],
        description = this[ExerciceTable.description]
    )
}