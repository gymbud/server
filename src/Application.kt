package xyz.gofdev

import io.ktor.application.*
import io.ktor.routing.*
import io.ktor.auth.*
import com.fasterxml.jackson.databind.*
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.locations.*
import com.mchange.v2.c3p0.*
import io.ktor.auth.jwt.jwt
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.NonceManager
import org.jetbrains.exposed.sql.*
import org.mariadb.jdbc.*
import xyz.gofdev.route.*
import xyz.gofdev.dao.*
import xyz.gofdev.security.customConfig
import java.security.MessageDigest
import java.sql.SQLException

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.main(testing: Boolean = false) {
    val dbHost = System.getenv("DB_HOST") ?: "localhost"

    val pool = ComboPooledDataSource().apply {
        driverClass = Driver::class.java.name
        jdbcUrl = "jdbc:mariadb://${dbHost}:3306/gymbud"
        user = "gymbud"
        password = "gymbud"
    }

    val db = Database.connect(pool)
    environment.monitor.subscribe(ApplicationStopped) { pool.close() }

    module(db)
}

fun Application.module(db: Database) {

    val userDao = UserDAOImpl(db)
    val loginDao = LoginDAOImpl(db)
    val exerciceDAO = ExerciceDAOImpl(db)
    val planDAO = PlanDAOImpl(db)
    val dayDAO = DayDAOImpl(db)
    val specificDAO = SpecificDAOImpl(db)

    install(Authentication) { jwt { customConfig(userDao) } }

    install(ContentNegotiation) { jackson { enable(SerializationFeature.INDENT_OUTPUT) } }

    install(Locations)

    install(StatusPages) {
        exception<ContentTransformationException> {
            call.respond(HttpStatusCode.BadRequest)
        }

        exception<SQLException> {
            when (it.errorCode) {
                1062 -> call.respond(HttpStatusCode.Conflict)
                else -> call.respond(HttpStatusCode.InternalServerError)
            }
        }
    }

    routing {
        auth(loginDao, userDao)
        users(userDao)
        exercice(exerciceDAO)
        plan(planDAO)
        day(dayDAO)
        specific(specificDAO)
    }
}

