package xyz.gofdev.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Plan(
    val id: Int,
    val name: String,
    val duration: Int,
    val description: String? = null,
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val owner: User? = null,
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val days: Collection<Day>? = listOf()
) {

    fun validate(): Collection<String> {
        val errors = mutableListOf<String>()
        if (name.isBlank()) {
            errors.add("Name cannot be empty")
        }

        if (duration < 1) {
            errors.add("Duration must be at least 1 week")
        }

        return errors
    }
}