package xyz.gofdev.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Day(
    val id: Int,
    val name: String,
    val description: String? = null,
    val plan: Plan? = null,
    @JsonProperty(value = "id_plan", access = JsonProperty.Access.WRITE_ONLY)
    val planId: Int? = null,
    val exercices: Collection<Specific>? = null
) {
    fun validate(): List<String> {
        val errors = mutableListOf<String>()

        if (name.isBlank()) {
            errors.add("Name cannot be empty")
        }

        if (planId == null || planId < 1) {
            errors.add("id_plan must match a plan")
        }

        return errors
    }
}

