package xyz.gofdev.model

data class Login(val mail: String, val pass: String)
