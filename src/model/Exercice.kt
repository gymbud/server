package xyz.gofdev.model

import com.fasterxml.jackson.annotation.*

data class Exercice(
    val id: Int,
    val name: String,
    val description: String? = null,
    @JsonIgnore
    var owner: User? = null
) {
    fun validate(): List<String> {
        val errors = mutableListOf<String>()

        if (name.isEmpty()) {
            errors.add("Name cannot be null")
        }

        return errors;
    }
}

