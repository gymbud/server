package xyz.gofdev.model

import at.favre.lib.crypto.bcrypt.BCrypt
import com.fasterxml.jackson.annotation.*
import io.ktor.auth.Principal

data class User(
    val id: Int,
    @JsonProperty("first_name")
    val firstName: String,
    @JsonProperty("last_name")
    val lastName: String,
    val email: String,
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    val password: String? = null
) : Principal {

    fun validate(): Collection<String> {
        val errors = mutableListOf<String>()

        if (firstName.isBlank()) {
            errors.add("First name must not be blank")
        }

        if (lastName.isBlank()) {
            errors.add("Last name must not be blank")
        }

        if (email.isBlank()) {
            errors.add("Email must not be blank")
        }

        if (password == null) {
            errors.add("Password must not be empty")
        } else {
            if (!password.contains(Regex("[0-9]+"))) {
                errors.add("Password must contain at least one number")
            }

            if (!password.contains(Regex("[A-Z]+"))) {
                errors.add("Password must contain at least one capital letter")
            }

            if (!password.contains(Regex("[a-z]+"))) {
                errors.add("Password must contain at least one lower case letter")
            }

            if (password.length < 8) {
                errors.add("Password must contain at least 8 characters")
            }
        }

        return errors
    }


    fun authenticate(login: Login): Boolean {
        return (
                this.email == login.mail &&
                        BCrypt
                            .verifyer()
                            .verify(login.pass.toCharArray(), this.password?.toCharArray())
                            .verified
                )
    }
}
