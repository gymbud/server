package xyz.gofdev.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Specific(
    val id: Int,
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val day: Day? = null,
    @JsonProperty(value = "id_day", access = JsonProperty.Access.WRITE_ONLY)
    val dayId: Int? = null,
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val exercice: Exercice,
    @JsonProperty(value = "id_exercice", access = JsonProperty.Access.WRITE_ONLY)
    val exerciceId: Int? = null,
    val sets: Int? = null,
    val reps: Int? = null,
    val tempo: String? = null,
    val duration: Int? = null,
    val rest: Int? = null
) {
    fun validate(): List<String> {
        val errors = mutableListOf<String>()

        if (exerciceId == null || exerciceId < 1) {
            errors.add("id_exercice must be bigger than 0")
        }

        if (dayId == null || dayId < 1) {
            errors.add("id_day must be bigger than 0")
        }

        if (sets == null && reps == null && tempo == null && rest == null) {
            if (duration == null || duration < 1) {
                errors.add("if sets, reps, tempo and rest are empty, duration must not be empty")
            }
        } else {
            if (duration != null) {
                errors.add("Duration must be null if any of sets, reps, tempo and rest aren't")
            }

            if(sets == null) {
                errors.add("Sets must not be null")
            }

            if (reps == null) {
                errors.add("Reps must not be null")
            }
        }

        return errors
    }
}

