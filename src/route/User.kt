package xyz.gofdev.route

import io.ktor.application.*
import io.ktor.http.HttpStatusCode
import io.ktor.locations.*
import io.ktor.request.ContentTransformationException
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import xyz.gofdev.dao.UserDAO
import xyz.gofdev.model.*
import java.sql.SQLException
import kotlin.Exception


fun Route.users(dao: UserDAO) {

    get<UserRoute> {
        val users: List<User>

        try {
            users = dao.getAll()
        } catch (e: Exception) {
            call.respond(HttpStatusCode.InternalServerError, e)
            return@get
        }

        call.respond(users)
    }

    get<UserRoute.ById> {
        val user = dao.getById(it.userId)

        if (user == null) {
            call.respond(HttpStatusCode.NotFound)
            return@get
        }

        call.respond(user)
    }

    post<UserRoute> {
        val user: User = call.receive()

        val errors = user.validate()

        if (errors.isNotEmpty()) {
            return@post call.respond(HttpStatusCode.BadRequest, mapOf("errors" to errors))
        }

        val createdUser = dao.create(user)
        call.respond(HttpStatusCode.Created, createdUser)
    }
}
