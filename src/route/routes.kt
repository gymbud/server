package xyz.gofdev.route

import io.ktor.locations.Location


@Location("/login")
class Login()

@Location("/logout")
class Logout()

@Location("/exercices")
class Exercices() {

    @Location("/{id}")
    data class ById(val id: Int, val parent: Exercices)
}

@Location("/specifics")
class Specifics() {
    @Location("/{id}")
    data class ById(val id: Int, val parent: Specifics)
}

@Location("/plans")
class Plans() {
    @Location("/{planId}")
    data class ById(val planId: Int, val parent: Plans) {
        @Location("/days")
        data class Days(val parent: Plans.ById)
    }
}

@Location("/days")
class Days() {
    @Location("/{dayId}")
    data class ById(val dayId: Int, val parent: Days) {
        @Location("/specifics")
        data class Specifics(val parent: Days.ById)
    }
}

@Location("/users")
class UserRoute {
    @Location("/{userId}")
    data class ById(val userId: Int, val parent: UserRoute) {
        @Location("/plans")
        data class Plans(val parent: UserRoute.ById)

    }
}
