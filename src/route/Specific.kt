package xyz.gofdev.route


import io.ktor.application.*
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.http.HttpStatusCode
import io.ktor.locations.*
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import xyz.gofdev.dao.SpecificDAO
import xyz.gofdev.model.*


fun Route.specific(dao: SpecificDAO) {

    get<Specifics> {
        call.respond(dao.getAll())
    }

    get<Specifics.ById> {
        call.respond(dao.getById(it.id) ?: HttpStatusCode.NotFound)
    }

    get<Plans.ById.Days> {
        call.respond(dao.getAllByPlanId(it.parent.planId))
    }


    authenticate {
        post<Specifics> {
            val user = call.authentication.principal<User>()!!
            val specific = call.receive<Specific>()
            val errors = specific.validate()

            if (errors.isNotEmpty()) {
                return@post call.respond(HttpStatusCode.BadRequest, mapOf("errors" to errors))
            }

            val owner = dao.getOwnerByDayId(specific.dayId!!)
                ?: return@post call.respond(HttpStatusCode.BadRequest)

            if (user.id != owner.id) {
                return@post call.respond(HttpStatusCode.Forbidden)
            }

            val newSpecific = dao.create(specific)

            call.respond(HttpStatusCode.Created, newSpecific)
        }

        delete<Specifics.ById> {

            val user: User = call.authentication.principal()!!

            val planOwner = dao.getOwnerById(it.id) ?: return@delete call.respond(HttpStatusCode.NotFound)

            if (planOwner.id != user.id) {
                return@delete call.respond(HttpStatusCode.Forbidden)
            }

            val deleted = dao.delete(it.id)

            if (!deleted) {
                return@delete call.respond(HttpStatusCode.NotFound)
            }

            return@delete call.respond(HttpStatusCode.OK)
        }
    }


}