package xyz.gofdev.route

import xyz.gofdev.dao.PlanDAO

import io.ktor.application.*
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.auth.jwt.JWTPrincipal
import io.ktor.http.HttpStatusCode
import io.ktor.locations.*
import io.ktor.request.ContentTransformationException
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import org.jetbrains.exposed.sql.appendTo
import xyz.gofdev.dao.UserDAO
import xyz.gofdev.model.*
import java.sql.SQLException
import kotlin.Exception

fun Route.plan(dao: PlanDAO) {

    get<Plans> {
        call.respond(dao.getAll())
    }

    get<Plans.ById> {
        call.respond(dao.getById(it.planId) ?: HttpStatusCode.NotFound)
    }

    get<UserRoute.ById.Plans> {
        call.respond(dao.getByUserId(it.parent.userId))
    }

    authenticate {
        post<Plans> {

            val owner = call.authentication.principal<User>()!!

            val plan = call.receive<Plan>()

            val errors = plan.validate()

            if (errors.isNotEmpty()) {
                call.respond(HttpStatusCode.BadRequest, mapOf("errors" to errors))
                return@post
            }

            val newPlan = dao.create(plan, owner)

            call.respond(newPlan)
        }

        delete<Plans.ById> {
            val user = call.authentication.principal<User>()!!

            val owner = dao.getOwnerById(it.planId)
                ?: return@delete call.respond(HttpStatusCode.NotFound)

            if (owner.id != user.id) {
                return@delete call.respond(HttpStatusCode.Forbidden)
            }

            dao.delete(it.planId)

            call.respond(HttpStatusCode.OK)
        }
    }
}