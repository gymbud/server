package xyz.gofdev.route

import xyz.gofdev.dao.ExerciceDAO

import io.ktor.application.*
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.http.HttpStatusCode
import io.ktor.locations.*
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import xyz.gofdev.model.*


fun Route.exercice(dao: ExerciceDAO) {

    get<Exercices> {
        call.respond(dao.getAll())
    }

    get<Exercices.ById> {
        call.respond(dao.getById(it.id) ?: HttpStatusCode.NotFound)
    }

    authenticate {
        post<Exercices> {

            val owner = call.authentication.principal<User>()!!

            val exercice = call.receive<Exercice>()

            val errors = exercice.validate()

            if (errors.isNotEmpty()) {
                call.respond(HttpStatusCode.BadRequest, mapOf("errors" to errors))
                return@post
            }

            val newExercice = dao.create(exercice, owner)

            call.respond(newExercice)
        }

        delete<Exercices.ById> {
            val user = call.authentication.principal<User>()!!

            val owner = dao.getOwnerById(it.id)
                ?: return@delete call.respond(HttpStatusCode.NotFound)

            if (owner.id != user.id) {
                return@delete call.respond(HttpStatusCode.Forbidden)
            }

            call.respond(HttpStatusCode.OK)
        }
    }
}