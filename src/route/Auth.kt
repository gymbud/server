package xyz.gofdev.route

import io.ktor.application.*
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.http.HttpStatusCode
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.respond
import io.ktor.routing.*
import xyz.gofdev.dao.*
import xyz.gofdev.model.*
import xyz.gofdev.security.JwtConfig
import java.util.*

fun Route.auth(loginDao: LoginDAO, userDao: UserDAO) {

    post<Login> {
        val login = call.receive<xyz.gofdev.model.Login>()

        val user = userDao.getByEmailWithPassword(login.mail)
            ?: return@post call.respond(HttpStatusCode.Unauthorized)

        if (!user.authenticate(login)) {
            return@post call.respond(HttpStatusCode.Unauthorized)
        }

        var token = loginDao.getToken(user)

        if (token != null && JwtConfig.verifier.verify(token).expiresAt > Date(System.currentTimeMillis())) {
            return@post call.respond(HttpStatusCode.OK, token)
        }

        token = JwtConfig.makeToken(user)
        loginDao.login(user, token)

        call.respond(HttpStatusCode.OK, token)
    }

    authenticate {
        get<Logout> {
            val user = call.authentication.principal<User>()!!

            loginDao.logout(user)

            call.respond(mapOf("message" to "You have been logged out"))

        }
    }
}