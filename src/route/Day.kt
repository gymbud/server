package xyz.gofdev.route

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.HttpStatusCode
import io.ktor.locations.*
import io.ktor.request.receive
import io.ktor.response.*
import io.ktor.routing.*
import xyz.gofdev.dao.DayDAO
import xyz.gofdev.model.Day
import xyz.gofdev.model.User

fun Route.day(dao: DayDAO) {

    get<Days> {
        call.respond(dao.getAll())
    }

    get<Days.ById> {
        call.respond(dao.getById(it.dayId) ?: HttpStatusCode.NotFound)
    }

    get<Plans.ById.Days> {
        call.respond(dao.getAllByPlanId(it.parent.planId))
    }

    authenticate {
        post<Days> {
            val user = call.authentication.principal<User>()!!

            val day = call.receive<Day>()

            val errors = day.validate()

            if (errors.isNotEmpty()) {
                return@post call.respond(HttpStatusCode.BadRequest, mapOf("errors" to errors))
            }

            val owner = dao.getOwnerByPlanId(day.planId!!)
                ?: return@post call.respond(HttpStatusCode.NotFound)

            if (owner.id != user.id) {
                return@post call.respond(HttpStatusCode.Forbidden)
            }

            val newDay = dao.create(day)

            call.respond(HttpStatusCode.Created, newDay)

        }

        delete<Days.ById> {
            val user = call.authentication.principal<User>()!!

            val owner = dao.getOwnerById(it.dayId)
                ?: return@delete call.respond(HttpStatusCode.NotFound)

            if (owner.id != user.id) {
                return@delete call.respond(HttpStatusCode.Forbidden)
            }

            dao.delete(it.dayId)

            call.respond(HttpStatusCode.OK)

        }
    }

}