package xyz.gofdev.security

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.application.ApplicationEnvironment
import io.ktor.auth.Principal
import io.ktor.auth.jwt.JWTAuthenticationProvider
import io.ktor.auth.jwt.JWTPrincipal
import xyz.gofdev.dao.UserDAO
import xyz.gofdev.model.User
import java.util.*

data class Token (val token: String)

object JwtConfig {
    const val issuer = "gymbud.xyz"
    private const val secret = "TtlJMFTvl3NHhuoJRxOtzwm7e90ULL1H"
    private const val validityInMs = 3_600_000 * 24 * 7 // a week
    private val algorithm = Algorithm.HMAC512(secret)

    val verifier: JWTVerifier = JWT.require(algorithm).withIssuer(issuer).build()

    fun makeToken(user: User): String = JWT.create()
        .withSubject("Authentication")
        .withIssuer(issuer)
        .withClaim("id", user.id)
        .withClaim("mail", user.email)
        .withExpiresAt(getExpiration())
        .sign(algorithm)

    private fun getExpiration() = Date(System.currentTimeMillis() + validityInMs)
}

fun JWTAuthenticationProvider.Configuration.customConfig(dao: UserDAO) {
    verifier(JwtConfig.verifier)
    realm = JwtConfig.issuer
    validate {
        with(it.payload) {
            val mail = getClaim("mail")
            val id = getClaim("id")
            if (mail.isNull || id.isNull || expiresAt < Date()){
                null
            }

            dao.getByMail(mail.asString())
        }
    }
}

